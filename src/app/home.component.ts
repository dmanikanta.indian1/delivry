import { Component, OnInit,HostListener } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule,FormControl } from "@angular/forms";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private modalService:NgbModal) { }

  ngOnInit(): void {
  }
  timein=new FormControl();
  subjectin=new FormControl();
  locationin=new FormControl();
  descriptionin=new FormControl();
  events: Array<any> = [
    {time: '08:00', subject: 'Breakfast with Simon', location: 'Lounge Caffe', description: 'Discuss Q3 targets'},
    {time: '08:30', subject: 'Daily Standup Meeting (recurring)', location: 'Warsaw Spire Office'},
    {time: '09:00', subject: 'Call with HRs'},
    {time: '12:00', subject: 'Lunch with Timmoty', location: 'Canteen', description: 'Project evalutation ile declaring a variable and using an if statement is a fine way to conditionally render a component, sometimes you might want to use a'},
  ];
  deleteEvent(event: any) {
    const itemIndex = this.events.findIndex(el => el === event);
    this.events.splice(itemIndex, 1);
  }

  open(mymodal) {
    this.modalService.open(mymodal);
}
addeve() {
  const newEvent: any = {
    time: this.timein.value,
    subject: this.subjectin.value,
    location: this.locationin.value,
    description: this.descriptionin.value
  };

  this.events.push(newEvent);

  this.timein.setValue('');
  this.subjectin.setValue('');
  this.locationin.setValue('');
  this.descriptionin.setValue('');

}






showScroll: boolean;
showMouse:boolean;
    showScrollHeight = 300;
    hideScrollHeight = 10;
    showMouseHeight=250;
    hideMouseHeight=10;




    @HostListener('window:scroll', [])
    onWindowScroll()
    {
      if (( window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > this.showScrollHeight)
      {
          this.showScroll = true;
      }
      else if ( this.showScroll && (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < this.hideScrollHeight)
      {
        this.showScroll = false;
      }


      if (( window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < this.showMouseHeight)
      {
          this.showMouse = true;
      }
      else if ( this.showScroll && (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > this.hideMouseHeight)
      {
        this.showMouse = false;
      }
    }



    scrollToTop()
    {
      (function smoothscroll()
      { var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0)
        {
          window.requestAnimationFrame(smoothscroll);
          window.scrollTo(0, currentScroll - (currentScroll / 5));
        }
      })();
    }
}
