import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
@Input() value:any;
@Output ()deleteeve :EventEmitter<any>=new EventEmitter<any>()
  constructor() { }

  ngOnInit(): void {
  }

  deleted() {
    this.deleteeve.emit(this.value);
  }
}
